/**
 * Load configs.
 */
require('dotenv').config()

/**
 * Run app.
 */
require('./src/app')
