const express = require('express')
const app = express()
const getEnv = require('./helpers/getEnv')
const errorHandler = require('errorhandler')
const bodyParser = require('body-parser')
const compression = require('compression')
const cors = require('cors')
const logger = require('morgan')

/**
 * Sentry configurations.
 */
require('@pf126/common/helpers/sentry').setup({
    name: 'order-code-service',
    express: app,
})

/**
 * Express configuration.
 */
app.set('trust proxy', 1)
app.disable('x-powered-by')
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(errorHandler())
app.use(cors())
app.use(logger('dev', {
    skip: function (req, res) {
        return req.originalUrl.includes('/heartbeat/')
    }
}))

const main = async () => {
    await require('./services/SettingServices').initialize()

    app.use('/api/order-code', require('./app.routes'))
    app.use(require('./app.routes'))

    /**
     * Start Express server.
     */
    const server = require('http').createServer(app)
    const port = getEnv('/port')
    server.listen(port, () => {
        console.log(`Listening on port ${port}...`)
    })
}

main()
    .catch(err => {
        console.error(err)
        process.exit(1)
    })
