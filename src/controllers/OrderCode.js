const { sendSuccess, sendError } = require('../providers/response')
const OrderCodeActions = require('../actions/OrderCodeActions')
const formatOrderCode = require('../helpers//formatOrderCode')

exports.generateOrderCode = async (req, res) => {
    OrderCodeActions.generateOrderCode()
        .then(formatOrderCode)
        .then(sendSuccess(req, res))
        .catch(sendError(req, res))
}