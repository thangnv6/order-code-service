const { getModel } = require('../connections/database')
const genPrefix = require('../helpers/genPrefix')

exports.generateOrderCode = async () => {
    const prefixes = process.env.PREFIXES ? process.env.PREFIXES.split(',') : []
    const OrderCode = getModel('OrderCode')
    const prefix = genPrefix(prefixes)
    return OrderCode.findOneAndUpdate({ prefix: prefix }, { $inc: { seq: 1 } }, { upsert: true, new: true })
}

