const express = require('express')
const router = express.Router()
const OrderCodeCtrl = require('./controllers/OrderCode')
const basicAuthOptions = require('./helpers/getBasicAuthOptions')()
const basicAuth = require('express-basic-auth')
const auth = basicAuth(basicAuthOptions)

router.get('/', (req, res) => res.send(`Hi! I'm order code service.`))
router.get('/ping', (req, res) => res.send('order-code-service:pong'))

router.get('/private/order-code', auth, OrderCodeCtrl.generateOrderCode)

module.exports = router
