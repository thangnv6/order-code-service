const postman = require('../connections/seller-postman')
const {global} = require('@pf126/setting-container')

/**
 * @type {GlobalSettings}
 */
module.exports = global.create({postman})