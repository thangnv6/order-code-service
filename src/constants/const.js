const PASSWORD_BASIC_AUTH = 'password_basic_auth'
const USER_BASIC_AUTH = 'user_basic_auth'

exports.PASSWORD_BASIC_AUTH = PASSWORD_BASIC_AUTH
exports.USER_BASIC_AUTH = USER_BASIC_AUTH