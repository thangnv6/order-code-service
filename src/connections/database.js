const { createMongoConnection } = require('@pf126/common').connectors
const getEnv = require('../helpers/getEnv')
const createStore = require('@pf126/backoffice-schemas')

const MONGODB_URI = getEnv('/mongodb')
console.log('MONGODB_URI:', MONGODB_URI)

const originConnection = createMongoConnection(MONGODB_URI, {
    poolSize: 1,
    debug: false,
})

module.exports = createStore(originConnection)
