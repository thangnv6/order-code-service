const Postman = require('@pf126/postman')
const getEnv = require('../helpers/getEnv')
const natsConfig = getEnv('/nats')

const postman = Postman(natsConfig, { name: 'ORDER_CODE_SERVICE', prefix: 'manager.srv.' })

module.exports = postman
