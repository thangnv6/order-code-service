'use strict'

const createResponse = require('@pf126/common/helpers/responseWithStatusCode')
const Logger = require('./logger')

module.exports = createResponse(Logger)
