const {createLogger} = require('@pf126/logger')

const loggerConfig = {
    level: process.env['LOG_LEVEL'] || 'error'
}

module.exports = createLogger(loggerConfig)
