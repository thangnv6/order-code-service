const randomer = require('randomstring')

const _generateText = () => {
    return randomer.generate({
        charset: 'ABDEGJKMNPQRVWXYZ',
        length: 1
    })
}

const genPrefix = (prefixes) => {
    if (!prefixes || !prefixes.length) {
        const text = _generateText()
        return `R${text}`
    }
    const random = Math.floor(Math.random() * prefixes.length)
    return prefixes[random]
}

module.exports = genPrefix
