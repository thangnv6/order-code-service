
const MAX_LENGTH_SEQ = 10
const JOIN_STRING = '0'

const formatOrderCode = (orderCode) => {
    const prefix = orderCode.prefix
    const seq = formatSeq(orderCode.seq)

    const code = `${prefix}-${seq}`
    return { code }
}

const formatSeq = (seq) => {
    const seqString = seq.toString()
    const length = seqString.length

    if (length >= MAX_LENGTH_SEQ) return seqStandardized(seqString)
    const newSeq = `${JOIN_STRING.repeat(MAX_LENGTH_SEQ - length)}${seqString}`
    return seqStandardized(newSeq)

}

const seqStandardized = (seq) => {
    const length = seq.length
    const mid = Math.ceil(length / 2)
    return `${seq.substring(0, mid)}-${seq.substring(mid, length)}`
}

module.exports = formatOrderCode