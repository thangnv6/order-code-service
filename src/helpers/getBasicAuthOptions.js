const SettingServices = require('../services/SettingServices')
const { USER_BASIC_AUTH, PASSWORD_BASIC_AUTH } = require('../constants/const')

module.exports = () => {
    let users = {}
    const username = SettingServices.getSetting(USER_BASIC_AUTH, '')
    const password = SettingServices.getSetting(PASSWORD_BASIC_AUTH, '')

    if (!username) throw new Error(`${USER_BASIC_AUTH} is required`)
    if (!password) throw new Error(`${PASSWORD_BASIC_AUTH} is required`)

    users[username] = password
    return {
        users: users,
        challenge: true
    }
}